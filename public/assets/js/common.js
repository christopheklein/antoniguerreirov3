var _paths = {};
_paths.requirejs = "../vendor/requirejs";
_paths.jquery = "../vendor/jquery/dist/jquery.min";
_paths.flexnav = "../vendor/flexnav/js/jquery.flexnav.min";
_paths.sticky = "../vendor/jquery-sticky/jquery.sticky";
_paths.cycle2 = "../vendor/jquery-cycle2/build/jquery.cycle2.min";
_paths.cycle2_carousel = "../vendor/jquery-cycle2/build/plugin/jquery.cycle2.carousel.min";
_paths.vegas = "../vendor/vegas/dist/vegas.min";

var _shim = {};
_shim.flexnav = ['jquery'];
_shim.sticky = ['jquery'];
_shim.cycle2 = ['jquery'];
_shim.cycle2_carousel = ['jquery', 'cycle2'];
_shim.vegas = ['jquery'];

requirejs.config({
	baseUrl: 'assets/js',
    paths: _paths,
    shim: _shim
});

require(['app/homepage']);
