<section id="prefooter">
	<div id="coordonnees">
		<div id="adresse"><img class="svg" src="assets/img/pin.svg" alt="Adresse"> Zone de Blancs Foss&eacute;s<br>10, rue des blancs Foss&eacute;s<br>51370 Ormes</div>
		<div id="tel"><img class="svg" src="assets/img/tel.svg" alt="T&eacute;l."> 03 26 86 59 49</div>
	</div>
</section>
<footer id="footer">
	<p id="agence"><img class="svg" src="assets/img/agence.svg" alt=""/></p>
	<p id="liens">Harum trium sententiarum nulli prorsus assentior. Nec enim illa prima vera est, ut, quem ad modum in se quisque sit, sic in amicum sit animatus. Quam multa enim, quae nostra causa numquam faceremus, facimus causa amicorum! <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a></p>
</footer>
