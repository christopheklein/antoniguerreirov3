<section id="teasers">
	<div id="competences">
		<h2>Nos domaines de comp&eacute;tence</h2>
		<p>Etude, fabrication, assemblage et maintenance. Faites confiance &agrave; groupe qui vous accompagne de A &agrave; Z</p>
	</div>
	<div id="slider_logo">
		<h2>Nos clients</h2>
		<div class="slideshow">
			<img src="assets/img/slider/logo/photo201.jpg">
			<img src="assets/img/slider/logo/photo202.jpg">
			<img src="assets/img/slider/logo/photo203.jpg">
			<img src="assets/img/slider/logo/photo204.jpg">
			<img src="assets/img/slider/logo/photo205.jpg">
			<img src="assets/img/slider/logo/photo206.jpg">
			<img src="assets/img/slider/logo/photo207.jpg">
			<img src="assets/img/slider/logo/photo208.jpg">
			<img src="assets/img/slider/logo/photo209.jpg">
			<img src="assets/img/slider/logo/photo210.jpg">
			<img src="assets/img/slider/logo/photo211.jpg">
			<img src="assets/img/slider/logo/photo212.jpg">
			<img src="assets/img/slider/logo/photo213.jpg">
			<img src="assets/img/slider/logo/photo214.jpg">
			<img src="assets/img/slider/logo/photo215.jpg">
		</div>
	</div>
</section>